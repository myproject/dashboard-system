const http = require('http');
const express = require('express');
const path = require("path");
const app = express();
const bodyParser = require('body-parser');

app.engine('pug', require('pug').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "/")));
app.use(bodyParser.json({limit: '10mb'}));
app.locals.basedir = __dirname;

/** routes **/
app.get("/index", function (req, res)
{
    res.render("index");
});

app.get("/", function (req, res)
{
    res.render("index");
});

/** include file **/
require("./backend/controller/systemCtrl.js")(app);

/** create server **/
http.createServer(app).listen(8080, function(){
    console.log("Express server listening on port " + 8080);
    console.log("http://localhost:8080")
});
