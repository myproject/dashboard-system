let app = angular.module('myApp', []);

/*
Small (smaller than 640px)
Medium (641px to 1007px)
Large (1008px and larger)
 */

app.controller("indexCtrl", ["$scope", "serviceSystemInfo", function ($scope, serviceSystemInfo) {
    //get canvas
    const canvas = document.getElementById("mainCanva");

    //settings
    let circleSize = window.innerWidth >= window.innerHeight ? window.innerWidth / 10 : window.innerHeight / 15;
    let fontSize = 20;

    //resize windows event
    window.onresize = () => {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        circleSize = window.innerWidth >= window.innerHeight ? window.innerWidth / 10 : window.innerHeight / 15;
        setupDisk();
    }

    /**
     * edit canvas with api information
     */
    let updateInfo = function() {
        serviceSystemInfo.getRamAndCpu().then((response) => {
            const cpuUsage = response["info"]["cpuUsage"];
            drawCpu(ctx, cpuUsage);

            const ramInfo = response["info"]["ram"];
            drawRam(ctx, ramInfo);
        });
    }


    //setup
    const ctx = canvas.getContext("2d");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    updateInfo();
    setInterval(updateInfo, 2000);

    let setupDisk = function() {
        serviceSystemInfo.getDisk().then((response) => {
            const diskInfo = response["info"];
            drawDisk(ctx, diskInfo);
        });
    }

    setupDisk();

    /**
     * drow a circle on a canva
     * @param x
     * @param y
     * @param color
     * @param angle
     * @param ctx
     */
    let drawCircle = function(x, y, color, angle, ctx) {
        ctx.strokeStyle = color;
        ctx.lineWidth = window.innerWidth > window.innerHeight ? window.innerWidth / 50 : window.innerHeight / 50;
        ctx.beginPath();
        ctx.arc(x, y, circleSize, -Math.PI / 2, angle - Math.PI / 2);
        ctx.stroke();
    }

    /**
     * write text on a canva
     * @param x
     * @param y
     * @param text
     * @param ctx
     * @param isTitle
     * @param isSubTitle
     */
    let drawText = function(x, y, text, ctx, isTitle = false, isSubTitle = false) {
        let size = fontSize;
        if(isTitle) {
            size *= 2;
        } else if(isSubTitle) {
            size /= 1.5;
        }
        ctx.font = size + 'px serif';
        ctx.fillStyle = "#FFFFFF";
        ctx.textAlign = "center";
        ctx.fillText(text, x, y);
    }

    let clearDraw = function(x, y, ctx) {
        ctx.clearRect(x - circleSize*1.5, y - circleSize*1.5, circleSize*2, circleSize*2);
    }

    /**
     * draw ram info on a canva
     * @param ctx
     * @param info
     */
    let drawRam = function(ctx, info) {
        let x;
        let y;
        if(window.innerWidth >= window.innerHeight) {
            x = canvas.width / 4;
            y = canvas.height / 2;
        } else {
            x = canvas.width / 2;
            y = canvas.height / 4;
        }
        clearDraw(x, y, ctx);
        drawText(x, y - circleSize * 1.5, "RAM", ctx, true);
        drawCircle(x, y, "#800000", 2 * Math.PI, ctx);

        const radian = ((2 * Math.PI) * info.used) / info.total;
        drawCircle(x, y, "#FF0000", radian, ctx);

        drawText(x, y, info.used + "/" + info.total + " GB", ctx);
    }

    /**
     * draw cpu info on a canva
     * @param ctx
     * @param info
     */
    let drawCpu = function(ctx, info) {
        let x;
        let y;
        if(window.innerWidth >= window.innerHeight) {
            x = canvas.width / 4 * 2;
            y = canvas.height / 2;
        } else {
            x = canvas.width / 2;
            y = canvas.height / 4 * 2;
        }
        clearDraw(x, y, ctx);
        drawText(x, y - circleSize * 1.5, "CPU", ctx, true);
        drawCircle(x, y, "#800000", 2 * Math.PI, ctx);

        const radian = ((2 * Math.PI) * info) / 100;
        drawCircle(x, y, "#FF0000", radian, ctx);

        drawText(x, y, info + "%", ctx);
    }

    /**
     * drow disk info on a canva
     * @param ctx
     * @param info
     */
    let drawDisk = function(ctx, info) {
        let x;
        let y;
        if(window.innerWidth >= window.innerHeight) {
            x = canvas.width / 4 * 3;
            y = canvas.height / 2;
        } else {
            x = canvas.width / 2;
            y = canvas.height / 4 * 3;
        }


        clearDraw(x, y, ctx);
        drawText(x, y - circleSize * 1.5, "DISK", ctx, true)
        drawCircle(x, y, "#800000", 2 * Math.PI, ctx);

        const radian = ((2 * Math.PI) * info.used) / info.size;

        drawCircle(x, y, "#FF0000", radian, ctx);

        const percentUsage = (100 * info.used) / info.size;
        drawText(x, y, Math.round(percentUsage *100) / 100 + "%", ctx);
        drawText(x, y + fontSize * 1.5, info.free + "GB free", ctx, false, true);
    }
}]);