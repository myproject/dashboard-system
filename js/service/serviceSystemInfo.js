app.service('serviceSystemInfo', function(serviceXhr) {
    this.getRamAndCpu = function(params) {
        let url = "/api/getInfo/ramAndCpu";
        return serviceXhr.post(url, params);
    }

    this.getDisk = function(params) {
        let url = "/api/getInfo/disk";
        return serviceXhr.post(url, params);
    }
});

