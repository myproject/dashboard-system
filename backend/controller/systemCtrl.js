const os = require("os");
const osu = require('node-os-utils');
const cds = require("check-disk-space").default;

module.exports = function (app) {
    const round2Dec = (n) => {return Math.round(n * 100) / 100;};
    const byteToGb = (byte) => {return round2Dec(byte / (1024 ** 3));};

    app.post("/api/getInfo/ramAndCpu", async function (req, res)
    {
        const totalRam = os.totalmem();
        const totalRamGb = byteToGb(totalRam);

        const freeRam = os.freemem();
        const freeRamGb =  byteToGb(freeRam);

        const usedRamGb = round2Dec(totalRamGb - freeRamGb);
        const ramInfo = {total: totalRamGb, free: freeRamGb, used: usedRamGb};

        const cpuUsage = round2Dec(await osu.cpu.usage());

        const result = {ram: ramInfo, cpuUsage: cpuUsage};

        res.status(200).send({info: result});
    });

    app.post("/api/getInfo/disk", async function (req, res) {
        let machineRoot = "";
        if (process.platform === "win32") {
            machineRoot = "C:/";
        } else {
            machineRoot = "/";
        }

        cds(machineRoot).then((diskSpace) => {

            let diskSize = byteToGb(diskSpace.size);
            let diskFree = byteToGb(diskSpace.free);
            let diskUsed = diskSize - diskFree;

            let diskInfo = {size: diskSize, free: diskFree, used: diskUsed}

            res.status(200).send({info: diskInfo});
        });
    });
}